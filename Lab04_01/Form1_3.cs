﻿using Lab04_01.MD;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lab04_01
{
    public partial class Form1_3 : Form
    {
        public Form1_3()
        {
            InitializeComponent();
            LoadFaculty();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        Model1 db = new Model1();

        private void LoadFaculty()
        {
            var faculties = db.Faculties.ToList();
            comboBox1.DataSource = faculties;
            comboBox1.DisplayMember = "FacultyName";
            comboBox1.ValueMember = "FacultyID";
        }

        private void BindGrid(List<Student> listStudent)
        {
            dataGridView1.Rows.Clear();
            foreach (var student in listStudent)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = student.StudentID;
                dataGridView1.Rows[index].Cells[1].Value = student.FullName;
                dataGridView1.Rows[index].Cells[2].Value = student.Faculty.FacultyName;
                dataGridView1.Rows[index].Cells[3].Value = student.AverageScore;
            }
        }




        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            var txtStudentvalue = textBox1.Text;
            var sinhvien = db.Students.SingleOrDefault(x => x.StudentID == txtStudentvalue);

            if (sinhvien != null)
            {
                textBox2.Text = sinhvien.FullName;
                comboBox1.SelectedValue = sinhvien.FacultyID;
            }
            else
            {
                textBox2.Text = "";
                comboBox1.SelectedIndex = -1;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                Model1 db = new Model1();
                var student = db.Students.SingleOrDefault(x => x.StudentID == textBox1.Text);

                if (student == null)
                {
                    throw new Exception("Không tìm thấy MSSV cần xóa!");
                }
                else
                {
                    DialogResult dr = MessageBox.Show("Bạn có muốn xóa ?", "YES/NO", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        db.Students.Remove(student);
                        db.SaveChanges();
                        MessageBox.Show("Xóa thành viên thành công!", "Thông báo", MessageBoxButtons.OK);

                        // Refresh the DataGridView
                        var listStudent = db.Students.ToList();
                        BindGrid(listStudent);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTim_Click(object sender, EventArgs e)
        {
            try
            {
                var studentId = textBox1.Text;
                var students = db.Students.Where(x => x.StudentID.Equals(studentId)).ToList();

                if (students.Count == 0)
                {
                    MessageBox.Show("Không tìm thấy thông tin SV!", "Thông báo", MessageBoxButtons.OK);
                }
                else
                {
                    BindGrid(students);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void btnXoa_Click(object sender, EventArgs e)
        {
            try
            {
                Model1 db = new Model1();
                var student = db.Students.SingleOrDefault(x => x.StudentID == textBox1.Text);

                if (student == null)
                {
                    throw new Exception("Không tìm thấy MSSV cần xóa!");
                }
                else
                {
                    DialogResult dr = MessageBox.Show("Bạn có muốn xóa ?", "YES/NO", MessageBoxButtons.YesNo);
                    if (dr == DialogResult.Yes)
                    {
                        db.Students.Remove(student);
                        db.SaveChanges();
                        MessageBox.Show("Xóa thành viên thành công!", "Thông báo", MessageBoxButtons.OK);

                        // Refresh the DataGridView
                        var listStudent = db.Students.ToList();
                        BindGrid(listStudent);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form1_3_Load(object sender, EventArgs e)
        {

        }
    }

}



