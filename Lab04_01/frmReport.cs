﻿using Lab04_01.MD;
using Lab04_01.Report.Studentreport;
using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Lab04_01
{
    public partial class frmReport : Form
    {
        public frmReport()
        {
            InitializeComponent();
        }

        private void frmReport_Load(object sender, EventArgs e)
        {
            Model1 db = new Model1();
            var ListStudentReportdbo = db.Students.Select(c => new StudentReportdbo
            {
                StudentID = c.StudentID,
                FullName = c.FullName,
                FacultyName = c.Faculty.FacultyName,
                AverageScore = c.AverageScore


            }).ToList();
            this.reportViewer.LocalReport.ReportPath = "Report.rdlc";
            this.reportViewer.RefreshReport();

            var reportDataSoure = new ReportDataSource("StudentDataSet", ListStudentReportdbo);
            this.reportViewer.LocalReport.DataSources.Clear();
            this.reportViewer.LocalReport.DataSources.Add(reportDataSoure);

            this.reportViewer.RefreshReport();
            this.reportViewer1.RefreshReport();
            this.reportViewer2.RefreshReport();
        }

        private void reportViewer_Load(object sender, EventArgs e)
        {

        }
    }
}
