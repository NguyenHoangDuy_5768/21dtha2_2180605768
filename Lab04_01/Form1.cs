﻿
using Lab04_01.MD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Lab04_01
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            dataGridView1.CellClick += new DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Model1 model = new Model1();
            var list = model.Faculties.ToList();
            var listStudent = model.Students.ToList();
            FillDataGrid(listStudent);
            FillComboboxKhoa(list);


        }




        private void FillDataGrid(List<Student> listStudent)
        {
            dataGridView1.Rows.Clear();
            foreach (var Student in listStudent)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = Student.StudentID;
                dataGridView1.Rows[index].Cells[1].Value = Student.FullName;
                dataGridView1.Rows[index].Cells[2].Value = Student.Faculty.FacultyName;
                dataGridView1.Rows[index].Cells[3].Value = Student.AverageScore;

            }
        }

        private void FillComboboxKhoa(List<Faculty> list)
        {

            comboBox1.DataSource = list;
            comboBox1.DisplayMember = "FacultyName";
            comboBox1.ValueMember = "FacultyID";

        }
        private void clear()
        {
            textBox1.Text = " ";
            textBox2.Text = "";
            textBox3.Text = "";
            comboBox1.SelectedIndex = 0;

        }

        private void button3_Click(object sender, EventArgs e)
        {

            var mssv = textBox1.Text;
            var ten = textBox2.Text;
            var khoa = comboBox1.Text;
            var dtb = textBox3.Text;
            if (mssv == "")
            {
                MessageBox.Show(Text, "MSSV không được bỏ trống.");
                return;
            }
            if (ten == "")
            {
                MessageBox.Show(Text, "Tên không được bỏ trống");
                return;
            }
            if (dtb == "")
            {
                MessageBox.Show(Text, "Điểm trung bình không được bỏ trống");
                return;
            }
            try
            {
                Student student = new Student();
                student.StudentID = mssv;
                student.FullName = ten;
                student.FacultyID = (int)comboBox1.SelectedValue;
                student.AverageScore = double.Parse(dtb);
                Model1 db = new Model1();
                db.Students.Add(student);
                db.SaveChanges();
                var listStudent = db.Students.ToList();
                FillDataGrid(listStudent);
                clear();
            }
            catch (Exception ex)
            {
                MessageBox.Show(Text, "Có lỗi xảy ra trong việc lưu sinh viên.");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowindex = e.RowIndex;
            textBox1.Text = dataGridView1.Rows[rowindex].Cells[0].Value.ToString();
            textBox2.Text = dataGridView1.Rows[rowindex].Cells[1].Value.ToString();
            comboBox1.Text = dataGridView1.Rows[rowindex].Cells[2].Value.ToString();
            textBox3.Text = dataGridView1.Rows[rowindex].Cells[3].Value.ToString();

        }


        private void button2_Click(object sender, EventArgs e)
        {
            Model1 db = new Model1();
            string studentid = textBox1.Text;
            double dtb = Convert.ToDouble(textBox3.Text);
            string hoten = textBox2.Text;

            int makhoa = Convert.ToInt32(comboBox1.SelectedValue.ToString());

            Student sinhvien = db.Students.FirstOrDefault(x => x.StudentID == studentid);

            if (sinhvien == null)
            {
                MessageBox.Show("Không thấy sinh viên có mã số " + studentid);
            }
            else
            {

                sinhvien.FullName = hoten;
                sinhvien.FacultyID = makhoa;
                sinhvien.AverageScore = dtb;

                db.SaveChanges();
                MessageBox.Show("Cập nhật dữ liệu tahnhf công.");
                db.SaveChanges();
                var listStudent = db.Students.ToList();
                FillDataGrid(listStudent);

            }
            clear();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            Model1 db = new Model1();
            string studentid = textBox1.Text;


            int makhoa = Convert.ToInt32(comboBox1.SelectedValue.ToString());

            Student sinhvien = db.Students.FirstOrDefault(x => x.StudentID == studentid);

            if (sinhvien == null)
            {
                MessageBox.Show("Không thấy sinh viên có mã số " + studentid);
            }
            else
            {

                db.Students.Remove(sinhvien);
                db.SaveChanges();
                MessageBox.Show("Xóa dữ liệu tahnhf công.");
                db.SaveChanges();
                var listStudent = db.Students.ToList();
                FillDataGrid(listStudent);

            }
            clear();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Form1_2 frm2 = new Form1_2();
            frm2.Show();
        }

        private void quảnLýKhoaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1_2 frm2 = new Form1_2();
            frm2.Show();
        }


        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Form1_3 frm3 = new Form1_3();
            frm3.Show();
        }

        private void tìmKiếmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form1_3 frm3 = new Form1_3();
            frm3.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
