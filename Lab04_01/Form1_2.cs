﻿using Lab04_01.MD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace Lab04_01
{

    public partial class Form1_2 : Form
    {
        public Form1_2()
        {
            InitializeComponent();
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
        }

        Model1 db = new Model1();

        private void BindGrid(List<Faculty> listFaculty)
        {
            dataGridView1.Rows.Clear();
            foreach (var item in listFaculty)
            {
                int index = dataGridView1.Rows.Add();
                dataGridView1.Rows[index].Cells[0].Value = item.FacultyID;
                dataGridView1.Rows[index].Cells[1].Value = item.FacultyName;
                dataGridView1.Rows[index].Cells[2].Value = item.TotalProfessor;
            }
        }



        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmFaculty_Load(object sender, EventArgs e)
        {
            List<Faculty> listFaculty = db.Faculties.ToList();
            BindGrid(listFaculty);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                Faculty newFaculty = new Faculty();

                bool a1 = int.TryParse(textBox1.Text, out int facultyId);
                bool a2 = int.TryParse(textBox4.Text, out int totalProfessor);
                if (!a1 || !a2)
                {
                    throw new Exception("Vui lòng nhập số hợp lệ cho FacultyID và TotalProfessor!");
                }

                newFaculty.FacultyID = facultyId;
                newFaculty.FacultyName = textBox2.Text;
                newFaculty.TotalProfessor = totalProfessor;

                db.Faculties.Add(newFaculty);
                db.SaveChanges();

                List<Faculty> listFaculty = db.Faculties.ToList();
                BindGrid(listFaculty);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }



        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Close();
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            int facultyId;
            bool success = int.TryParse(textBox1.Text, out facultyId);
            if (success)
            {
                Faculty facultyToRemove = db.Faculties.Find(facultyId);
                if (facultyToRemove != null)
                {
                    db.Faculties.Remove(facultyToRemove);
                    db.SaveChanges();

                    List<Faculty> listFaculty = db.Faculties.ToList();
                    BindGrid(listFaculty);
                }
                else
                {
                    MessageBox.Show("Không tìm thấy mã khoa!", "Thông báo", MessageBoxButtons.OK);
                }
            }
            else
            {
                MessageBox.Show("Mã khoa phải là một số!", "Thông báo", MessageBoxButtons.OK);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }

}
